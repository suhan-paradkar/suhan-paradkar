## Hi there 👋
- 🔭 I’m currently a Pharmacy student
- 🤔 I’m currently busy setting up python bots

[![Anurag's GitHub stats](https://github-readme-stats.vercel.app/api?username=suhan-paradkar)](https://github.com/anuraghazra/github-readme-stats)

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=suhan-paradkar&layout=compact)](https://github.com/anuraghazra/github-readme-stats)

[![GitHub Streak](https://github-readme-streak-stats.herokuapp.com/?user=suhan-paradkar&theme=high-contrast)](https://git.io/streak-stats)
